import React, {useEffect, useState} from 'react';
import {Button, Grid, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchMessages} from "../../store/actions/messagesActions";
import MessageItem from "../../components/MessageItem/MessageItem";
import {Link} from "react-router-dom";
import Modal from "../../components/UI/Modal/Modal";
import NewMessage from "../NewMessage/NewMessage";

const Messages = () => {
    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages.messages);
    const [error, setError] = useState(null);
    const dismiss = () => {
        setError(null);
    };

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);

    return (
        <Grid container direction="column" spacing={2}>
            <Modal show={Boolean(error)} close={dismiss}>
                <NewMessage/>
            </Modal>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item>
                    <Typography variant="h4">Messages</Typography>
                </Grid>
                <Grid item>
                    <Button color="primary" component={Link} to="/messages/new">Add new message</Button>
                </Grid>
            </Grid>
            <Grid item container direction="column" spacing={1}>
                {messages.map(message => (
                    <MessageItem
                        key={message.id}
                        id={message.id}
                        author={message.author}
                        message={message.message}
                        image={message.image}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default Messages;
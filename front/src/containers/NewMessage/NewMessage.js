import React, {useState} from "react";
import Typography from "@material-ui/core/Typography";
import MessageForm from "../../components/MessageForm/MessageForm";
import {createMessage} from "../../store/actions/messagesActions";
import {useDispatch} from "react-redux";
import Modal from "../../components/UI/Modal/Modal";

const NewMessage = ({history}) => {
    const dispatch = useDispatch();
    const [error, setError] = useState(true);

    const onSubmit = async messageData => {
        await dispatch(createMessage(messageData));
        history.replace('/');
    };

    const dismiss = () => {
        setError(null);
    };

    return (
        <>
            <Modal show={Boolean(error)} close={dismiss}>
                <Typography variant="h6">New message</Typography>
                <MessageForm
                    onSubmit={onSubmit}
                />
            </Modal>
        </>
    );
};

export default NewMessage;
import {Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Messages from "./containers/Messages/Messages";
import NewMessage from "./containers/NewMessage/NewMessage";

const App = () => (
  <Layout>
    <Switch>
      <Route path="/" exact component={Messages}/>
      <Route path="/messages/new" component={NewMessage}/>
    </Switch>
  </Layout>
);

export default App;

import {
  FETCH_MESSAGE_FAILURE,
  FETCH_MESSAGE_REQUEST,
  FETCH_MESSAGE_SUCCESS,
  FETCH_MESSAGES_FAILURE,
  FETCH_MESSAGES_REQUEST,
  FETCH_MESSAGES_SUCCESS
} from "../actions/messagesActions";

const initialState = {
  fetchLoading: false,
  singleLoading: false,
  messages: [],
  message: null,
};

const messagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MESSAGES_REQUEST:
      return {...state, fetchLoading: true};
    case FETCH_MESSAGES_SUCCESS:
      return {...state, fetchLoading: false, messages: action.payload};
    case FETCH_MESSAGES_FAILURE:
      return {...state, fetchLoading: false};
    case FETCH_MESSAGE_REQUEST:
      return {...state, singleLoading: true};
    case FETCH_MESSAGE_SUCCESS:
      return {...state, singleLoading: false, message: action.payload};
    case FETCH_MESSAGE_FAILURE:
      return {...state, singleLoading: false};
    default:
      return state;
  }
};

export default messagesReducer;
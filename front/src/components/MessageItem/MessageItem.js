import {Grid, Typography} from "@material-ui/core";
import {apiURL} from "../../config";

const MessageItem = ({author, message, image}) => {

    return (
        <Grid item xs={12} sm={12} md={12} lg={12}>
            <Typography>Автор: <b>{author}</b></Typography>
            <Typography>Сообщение: <i>{message}</i></Typography>
            {image ? <img src={apiURL + '/uploads/' + image} alt="photoCard" width="150" height="150"/> : ''}
            <hr/>
        </Grid>
    );
};

export default MessageItem;
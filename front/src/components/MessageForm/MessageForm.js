import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import FileInput from "../UI/FileInput/FileInput";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2)
  },
}));

const MessageForm = ({onSubmit}) => {
  const classes = useStyles();

  const [state, setState] = useState({
    author: "",
    message: "",
    image: null,
  });

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();
    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    onSubmit(formData);
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];
    setState(prevState => {
      return {...prevState, [name]: file}
    });
  };

  return (
    <Grid
      container
      direction="column"
      spacing={2}
      component="form"
      className={classes.root}
      autoComplete="off"
      onSubmit={submitFormHandler}
    >
      <Grid item xs>
        <TextField
          label="Author"
          name="author"
          value={state.title}
          onChange={inputChangeHandler}
        />
      </Grid>

      <Grid item xs>
        <TextField
          required
          multiline
          rows={3}
          label="Message"
          name="message"
          value={state.description}
          onChange={inputChangeHandler}
        />
      </Grid>

      <Grid item xs>
        <FileInput
          label="Image"
          name="image"
          onChange={fileChangeHandler}
        />
      </Grid>

      <Grid item xs>
        <Button type="submit" color="primary" variant="contained">Save</Button>
      </Grid>
    </Grid>
  );
};

export default MessageForm;